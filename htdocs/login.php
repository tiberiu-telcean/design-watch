<?php
    if($_SERVER['REQUEST_METHOD']!=='POST'){ // 
        header("405 Method Not Allowed",true,405);
        exit;
    }
    if(!(isset($_POST['username'])||isset($_POST['password']))){
	header("417 POST Data not submitted",true,417);
        exit;
    }
?>
<?php
	function generateRandomString($length = 10) {
    		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    		$charactersLength = strlen($characters);
   		$randomString = '';
    		for ($i = 0; $i < $length; $i++) {
        		$randomString .= $characters[rand(0, $charactersLength - 1)];
    		}
    		return $randomString;
	}
	include('../includes/getserveraddress.inc.php');	
	include("../includes/connection.inc.php");
	$result=get("SELECT * FROM `usern` WHERE usernn LIKE '".mysqli_real_escape_string($con,$_POST['username'])."'");
	if($usern = $result->fetch_array()){
		$result=get("SELECT * FROM `passwordd` WHERE `id` LIKE '".$usern['id']."'");
		$row = $result->fetch_array();
		$pass = $_POST['password'];
		$hashed = $row['hashed'];
 		if( password_verify($pass, $hashed) ){
			$result=get("SELECT * FROM cookies  WHERE id LIKE '".$usern['id']."'");
            		$cookie=$result->fetch_array();
			setcookie("idl", password_hash($cookie['cookie'], PASSWORD_DEFAULT), time() + 60 * 60 * 24 * 30); // Account id    : expires in a month
			setcookie("name", $usern['usernn'], time() + 60 * 60 * 24 * 30); // Account name  : expires in a month
			header("Location: /", true, 301);
		 } else {
			echo "Wrong password <br><a href='http://$serve/login.html'>Try again</a>"; // In case the user typed the password wrong
		 }
	} else {
    echo "Invalid username <br><a href='http://$serve/login.html'>Try again</a>"; // In case the username doesn't exist or is unacceptable
	}
	mysqli_close($con);
?>
