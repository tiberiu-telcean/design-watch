<?php
  if(!isset($_COOKIE['csrf'])){
      echo "You need to be logged in";
      exit;
  }
?>

<?php
  function xors($a,$b){
      for($i=0;i<strlen($a);$i++){
          $a[$i]^=$a[$b];
      }
  }
?>

<?php
  $csrf_temp=file_get_contents("../csrf-tok-temp.txt");
  $csrf_acct=$_COOKIE['csrf'];
  if($detect){
      $csrf=false;
  }else{
      $csrf=xors($csrf_acct,$csrf_temp);
  }
  if($_POST['csrf']===xors($csrf_acct,$csrf_temp)&&$detect){
      echo "ACCESS GRANTED";
      $csrf=true;
  }
?>
