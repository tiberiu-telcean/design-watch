<?php
    function redir(){
        header("Location: http://localhost/edit_profile.php", true, 301);
    }
    if($_SERVER['REQUEST_METHOD']=='POST'){
        if(!(isset($_POST['disp']))){
            redir();
	}
	include('../includes/connection.inc.php');
	include('../includes/check_cookie.inc.php');
	if(isset($row['id'])){
            $target_file = "pfp/".strval($row['id']).".jpg";
            $uploadOk = 1;
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    $uploadOk = 0;
                }
            }
            if ($uploadOk == 0) {
                header("HTTP/1.0 400 Bad Request");
                exit;
            } else {
                move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                get("UPDATE usern SET disp = '".escape($_POST['disp'])."' WHERE id = ".$row['id']);
                header("Location: /profile.php?id=".$_COOKIE['name'], true, 301);
            }
        }
    }elseif($_SERVER['REQUEST_METHOD']=='GET'){
        redir();
    }else{
        header("HTTP/1.0 400 Bad Request");
    }
?>
