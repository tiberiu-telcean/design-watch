function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}
//stackoverflow code ^^^

function toggle_vis(el){
	if(el.style.display===""){
		el.style.display="none";
		return;
	}
	if(el.style.display==="block"){
	    el.style.display="none";
	}else{
        el.style.display="block";
	}
}
function select_lang(lang){
	setCookie('lang', lang, 7);
	window.location.reload(true); 
}

$(document).ready(function(){

    $(".menu-clicker").on("click", function(){
        
        var dropdown = document.getElementsByClassName("mobile-dropdown")[0];
        toggle_vis(dropdown);

    });
    $(".lang-clicker").on("click", function(){
        var dropdown = document.getElementsByClassName("mobile-dropdown")[0];
    	dropdown.style.display="block";
        if(dropdown.tab==="menu"){
            dropdown.tab="lang";
        }else{
            dropdown.tab="menu";
        }
        for(var i=0;i<dropdown.childElementCount;i++){
        	toggle_vis(dropdown.children[i]);
        }
    });
});
