<?php
    include("../includes/lang.inc.php");
    include("../includes/connection.inc.php");
    $tran = get_trans('title',$lang);
    $profile = get_trans('profile', $lang);
    $upload_image = get_trans('upload_image', $lang);
    $csec = get_trans('csec', $lang);
    $logout = get_trans('logout', $lang);
    $sects = get("SELECT * FROM `sect`");
    $watch = get_trans('watch', $lang);
    $by = get_trans('by', $lang);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Design watch</title>
        <link rel="stylesheet" href="watch.css">
        <script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous"></script>
	<script src="menu.js"></script>
    </head>
    <body>
	<header>
            <h1><?=$tran?></h1>
                  <button class="mbut lang-clicker"
                         style="float:right;"><img src="assets/images/globe.svg" class="mbut inverted"></button>
            <?php 
                  if ( ! isset( $_COOKIE[ "idl" ] ) ) {     ?> 
                    <a href="signup.html" >Sign up</a>
                    <a href="login.html" >Log in</a>
            <?php }else{ ?>
                    <a href="profile.php?id=<?=urlencode($_COOKIE["name"])?>"
		        style="float:right;"><?=$profile?></a>
                    <a href="upload_image.php" 
			style="float:right`;"><?=$upload_image?></a>
                    <a href="create_section.html" 
		    	style="float:right`;"><?=$csec?></a>
                    <a href="logout.php" 
		        style="float:right`;"><?=$logout?></a>
                    <nav class="menu-nav">
                        <a class="menu-clicker"><img src="assets/images/menu.png" class="menu-icon">
                        <ul class="mobile-dropdown" style="display:none;">
                            <li><a href="profile.php?id=<?=urlencode($_COOKIE["name"])?>"
                           style="float:right;">Profile </a></li>
                           <li><a href="upload_image.html" 
                           style="float:right;">Upload image </a></li>
                           <li><a href="logout.php" 
                           style="float:right;">Log out</a></li>
                           <li style="display:none;"><button onclick="select_lang('en')" class="drpdwn"
                           style="float:right;">English</button></li>
                           <li style="display:none;"><button onclick="select_lang('ja')" class="drpdwn"
			   style="float:right;">Japanese</button></li>
			   <li style="display:none;"><button onclick="select_lang('ro')" class="drpdwn"
                           style="float:right;">Romanian</button></li>
			   <li style="display:none;"><button onclick="select_lang('ln')" class="drpdwn"
                           style="float:right;">Latin</button></li>
			</ul>
                    </nav>
            <?php } ?>
	</header>
	<ul>
            <?php	
	        while($row = mysqli_fetch_array($sects)) {
			$id=$row["id"];
			$name=$row["name"];
			$auth = mysqli_fetch_array(get("SELECT * FROM `sect_auth` WHERE `sid`='$id'; "));
			$authname = $auth["auth"];
			$mess = str_replace("%s", $name, $watch);
			echo "<li>
				<a href=\"watch.php?sect=$id\">
				<img style=\"max-height:19px;border:none;\" src=\"/pfp.php?id=$id&type=sect\">$mess </a>
				<span class=\"username\">$by <a href=\"profile.php?id=$authname\">$authname</a></span>
			      </li>";
		}
            ?>    
        </ul>
    </body>
</html>
