<?php
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[];\'\\,./{}:"|<>?!@#$%^&*()_+-=';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    include("../includes/connection.inc.php");
    $secret = generateRandomString();
    $user = escape($_POST['username']);
    $pass = escape($_POST['password']);
    $hashed = password_hash($pass, PASSWORD_DEFAULT);
    get("INSERT INTO `cookies` (`cookie`) VALUES ('".hash_hmac("SHA256",$user.$pass,$secret)."')");
    get("INSERT INTO `passwordd` (`secret`, `hashed`) VALUES ('".$secret."', '".$hashed."')");
    get("INSERT INTO `usern` (`usernn`, `disp`) VALUES ('".$user."', '".$user."');");
    header("Location: /login.html", true, 301);
    mysqli_close($con);
?>
