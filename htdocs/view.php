<?php
    include("../includes/lang.inc.php");
    include("../includes/connection.inc.php");
    include("../includes/getserveraddress.inc.php");
    $sect = $_GET['sect'];
    $id = $_GET['id'];
    $result=get("SELECT * FROM `sect` WHERE id = '".mysqli_real_escape_string($con,$sect)."'");
    $comments=get("SELECT * FROM `comment` WHERE post = '$id' AND post_sect='$sect'; ");
    $row = mysqli_fetch_array($result);
    $tran = get_trans('like', $lang);
    $com = get_trans('comments', $lang);
    if(isset($_GET['high'])){
	    $highlighted = escape($_GET['high']);
    }else{
	    $highlighted = false;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="watch.css">
        <script>
            function like(){
                document.location.href="http://<?php echo $serve;?>/like.php"
            }
        </script>
    </head>
    <body>
	<header>
	    <h1>View image: id <?=htmlentities($id, ENT_QUOTES)?>  from section <?=htmlentities($row['name'], ENT_QUOTES)?></h1>
        </header>
        <?php
            echo "
                <div id='imgcont'>
                    <img src='/static.php?sect=".htmlentities($sect, ENT_QUOTES)."&id=".htmlentities($id, ENT_QUOTES)."'>
                    <hr>
                </div>";
        ?>
	<div><h1 style="color:black;position:relative"><?=$com?></h1></div>
	<form action="upload_comment.php" method="POST" style="position:relative;">
	    <textarea name="content"></textarea>
	    <input type="hidden" name="sect" value="<?=$sect?>"></input>
	    <input type="hidden" name="id" value="<?=$id?>"></input>
	    <input type="submit" name="submit" value="Post"></input>
	</form>
<?php
	if(!($highlighted==false)){
		$high_comment=mysqli_fetch_array(get("SELECT * FROM `comment` WHERE post = '$id' AND post_sect='$sect' AND `id`=$highlighted; "));
    	if(!($high_comment==false)){
    	$irow = mysqli_fetch_array(get("SELECT * FROM `usern` WHERE `usernn`='".$high_comment['auth']."';"));
	echo "<div class=\"comment\">
			    <span class=\"username\">Highlighted</span>
                            <p style=\"position:relative;\">
                                ".nl2br(htmlentities($high_comment['text'], ENT_QUOTES))."
                            </p>  
                            <span class=\"username\" style=\"position:relative;\">
                                by <a href=\"profile.php?id=".$high_comment['auth']."\"><img style=\"max-height:19px;border:none;\" src=\"/pfp.php?id=".$irow['id']."&type=post\">".$high_comment['auth']."</a>
                            </span>
			  </div>";
	}
	}
    	if($comments!=false){
		while($row=mysqli_fetch_array($comments)){
		    if($row['id']===$highlighted){
		        continue;
		    }
		    $irow = mysqli_fetch_array(get("SELECT * FROM `usern` WHERE `usernn`='".$row['auth']."';"));
		    echo "<div class=\"comment\">
			    <p style=\"position:relative;\">
				".nl2br(htmlentities($row['text'], ENT_QUOTES))."
			    </p>
			    <span class=\"username\" style=\"position:relative;\">
				by <a href=\"profile.php?id=".$row['auth']."\"><img style=\"max-height:19px;border:none;\" src=\"/pfp.php?id=".$irow['id']."&type=post\">".$row['auth']."</a>
			    </span>
			  </div>";
	    }
	}
	?>     
	</body>
</html>
