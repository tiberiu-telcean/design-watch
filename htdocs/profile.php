<?php
    include('../includes/connection.inc.php');
    include('../includes/lang.inc.php');
    $result=get("SELECT * FROM usern WHERE usernn='".escape($_GET['id'])."';");
    $row=$result->fetch_array();
    $comments=get("SELECT * FROM `comment` WHERE auth='".escape($_GET['id'])."'");
    $by = get_trans("by", $lang);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-width=1.0">
        <link rel="stylesheet" href="watch.css">
    </head>
    <body>
        <header>
            <h1 class="leftmost">
                <?php 
                    if($row===NULL){
                        echo "Username '".
                            htmlentities($_GET['id'],ENT_QUOTES).
                            "' does not exist</h1></header>";
                        exit;
                    }else{
                        echo htmlentities($row['disp'],ENT_QUOTES)."'s profile";
                    }
                ?>
            </h1>
            <?php
                if(isset($_COOKIE['idl'])&&isset($_COOKIE['name'])){
                    if($_COOKIE['name']===$_GET['id']){
                        echo "
                        <a href='edit_profile.php' id='edit' >
                            <img src='assets/images/pencil.png' width='24' height='24'>
                            
                            Edit profile
                        </a>";
                    }
                }
            ?>
        </header>
        <content>
            <?php
                if($row!==NULL){
                    echo '<div id="imgcont">
                        <img width="512" height="256" src="pfp.php?id='.
                                strval($row['id']).
                            '&type=post" id="prof" class="leftmost">
                        
                        <h2 class="dispname">';
                        if($row['disp']===''){
                            echo htmlentities($row['usernn'],ENT_QUOTES);
                        }else{
                            echo htmlentities($row['disp'],ENT_QUOTES);
                        }
                        echo '</h2>
                            <h5 class="username">@'.htmlentities($row['usernn'],ENT_QUOTES).'</h5>
		    <hr>
		    </div>';
                }
	?>
	<div><h1 style="color:black;position:relative">Comments by <?=$row['disp']?></h1></div>
	<?php
	if($comments!=false){
		while($crow=mysqli_fetch_array($comments)){
			echo "<div class=\"comment\">
				<span>On <a href=\"/view.php?id=".$crow['post']."&sect=".$crow['post_sect']."&high=".$crow['id']."\">Id ".$crow['post']." sect ".$crow['post_sect']."</a>
				</span>
				<p style=\"position:relative;\">".nl2br(htmlentities($crow['text'], ENT_QUOTES))."</p>
				<span class=\"username\" style=\"position:relative;\">$by <a href=\"/profile.php?id=".$crow['auth']."\">".$crow['auth']."</a>
				</span>
			      </div>";
		}
	}
	?>
	</content>
    </body>
</html>
