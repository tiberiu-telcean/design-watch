<?php
    include('../includes/connection.inc.php');
    $important = true; include('../includes/check_cookie.inc.php'); // exit if not logged in
    if($_SERVER['REQUEST_METHOD']!=='POST'){
	$sects = get("SELECT * FROM `sect`");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="watch.css">
    </head>
    <body>
        <header><h1>Upload an image</h1></header>
        <form method="POST" action="upload_image.php" enctype="multipart/form-data">
            <input type="file" name="image"><br>
	    <select name="sect">
		<?php
			while($row = mysqli_fetch_array($sects)) {
				$id = $row['id'];
				$name = $row['name'];
                		echo '<option value="'.$id.'" label="'.$name.'">'.$name.'</option>';
                	}
		?>
            </select><br>
            <input type="Submit" value="Upload" name="Submit">
        </form>
    </body>
</html>
<?php 
    } 
?>
<?php
    if($_SERVER['REQUEST_METHOD']==='POST'){
        $target_dir = $_POST['sect']."res/";
        $a=intval(file_get_contents($_POST['sect']."res/lat.txt"));
        $target_file = $target_dir . strval($a+1) .'.jpg';
        file_put_contents($_POST['sect']."res/lat.txt",strval($a+1));
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
        if ($_FILES["image"]["size"] > 500000000) {
            $uploadOk = 0;
        }
        if($imageFileType != "jpg") {
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            header("HTTP/1.0 400 Bad Request");
            exit;
        } else {
            $result=get("SELECT * FROM usern WHERE id=".strval($row['id']));
            $row=mysqli_fetch_array($result);
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
            get("INSERT INTO author(id,sect,auth) VALUES (".strval(intval($a)+1).",'".$_POST['sect']."','".$row['usernn']."')");
            header("Location: /view.php?id=".file_get_contents($_POST['sect']."res/lat.txt")."&sect=".$_POST['sect'], true, 301);
        }
    }else{
        header("HTTP/1.0 400 Bad Request");
    }
?>
