<?php
    function report($reason) { //report problem in case a server error occurred
        file_put_contents("report.txt",file_get_contents("reports.txt")."\n".$reason);
    }
    $con=mysqli_connect("127.0.0.1","root","toor","users");
    if (mysqli_connect_errno()){
        echo "Database not working, reporting right now!";
        report("--database ".mysqli_connect_error()."--");
        exit;
    }
    function get($query){
	global $con;
        return mysqli_query($con, $query);	    
    }
    function escape($string){
	global $con;
        return mysqli_real_escape_string($con, $string);	    
    }
?>
