#!/bin/bash
[ ! "$(whoami)" = "root" ] && echo "Are you sure you are root?" && exit
SCRIPT=`realpath $0`
SPATH=`dirname $SCRIPT`
$SPATH/setup.sh
AP=/opt/lampp/app
mkdir -p $AP && cp -r $SPATH/* $AP

rm $AP/* # Doesn't remove directories such as htdocs

chmod 777 $AP/htdocs -R
# Please change document root on apache.conf to /opt/lampp/app/htdocs
